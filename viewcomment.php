<?php
session_start();
$conn=new MYSQLi("localhost","root","","comments");
if($conn){
	echo "";
}
$sql="select * from comment";
$res=$conn->query($sql);
?>
 <!DOCTYPE html>
<html>
<head>
	<title>Home Page</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >
	<link rel="stylesheet" href="styles.css" >
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
</head>
<body>


<div class="container">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">Comments</div>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Comment id</th>
						<th>Name</th>
						<th>Comment</th>
						<th>Time</th>
					</tr>
				</thead>
				<tbody>
				<?php
					while ( $r = mysqli_fetch_assoc($res)) {
				?>
					<tr>
						<td><?php echo $r['id']; ?></td>
						<td><?php echo $r['username'] ?></td>
						<td><?php echo $r['comment']; ?></td>
						<td><?php echo $r['timestamp']; ?></td>
						<?php if($_SESSION['userName']==$r['username'])

					{	 ?>
						<td><a href="editcomment.php?id=<?php echo $r['id']; ?>">Edit</a>   <a href="delcomment.php?id=<?php echo $r['id']; ?>">Del</a></td>
<?php }?>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>

	</div>
</div>
</body>
</html>
